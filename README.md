##### Création d'une page 'Mini Jeu'

    Cette page contient les mini jeux que j'ai codé, vous y trouverez deux jeux :
    - Le Morpion
    - Le Floppy Ball

Elle est codée avec du HTML et CSS par BOOTSRTAP. 
Les jeux eux sont codés avec TYPESCRIPT CSS et HTML


##### Morpion 

    La règle du jeu est de pouvoir alligner les croix ou les ronds en touchant les zone voulu pour afficher les croix/ronds, c'est un jeu à 2 joueurs. Il y a la possibilité de pouvoir y rejouer 


##### Floppy Ball

    La règle ici, est que la balle doit passer dans le carré blanc et ne doit pas toucher les lignes bleues. Pour faire sauter la balle il faut toucher avec la souris n'importe ou sur l'écran. Le joueur pourra voir le score qu'il a fait.

##### Liens

liens gitlab : https://gitlab.com/Ellyesse691/game-js 
Mail : Ellyesse.daoudi@gmail.com


Ce projet mobilise HTML & CSS, le framework Boostrap et TypeScript. Il est consultable sur gitlab
Une fois le projet proche de son aboutissement, je l'ai déployé via Netlify cf. 
