let poteau:any = document.querySelector<HTMLElement>('#poteau')
let trou:any = document.querySelector<HTMLElement>('#trou')
let perso:any = document.querySelector<HTMLDivElement>('#perso')
const score = document.querySelector<HTMLBodyElement>('.score')
let saut:number = 0;
let compteur:number = 0;


// Permet la disposition aléatoire des lignes bleues et du block blanc dans le canvas.
trou?.addEventListener('animationiteration', () => {
    let random = Math.random()*3;
    let top = (random*100)+150
    if(trou){
        trou.style.top = -(top) +"px";
    };
    compteur++;
});

setInterval(function(){
    let persoTop = parseInt(window.getComputedStyle(perso).getPropertyValue("top"));
    if(saut==0){
        //Lanimation de la chute de la balle
    if(perso){
        perso.style.top = (persoTop + 6)+"px";
    }}
    let poteauLeft = parseInt(window.getComputedStyle(poteau).getPropertyValue("left"));
    let trouTop = parseInt(window.getComputedStyle(trou).getPropertyValue("top"));
    let persoTopi = parseInt(window.getComputedStyle(perso).getPropertyValue("top"));
    let cTop = -(700-persoTopi);
    //Position de la balle et condition de défaite
    if((persoTopi>700)||(poteauLeft<20)&&(poteauLeft>-50)&&((cTop<trouTop)||(cTop>trouTop+150))){
        alert("GAME OVER ! Score :" + compteur)
        if(perso)
        perso.style.top = 150 + "px";
        compteur = 0;
    }

    if(score){
        score.innerHTML = 'SCORE : ' + compteur
    }
},10);


// Cette fonction permet de faire sauter la balle
function Sauter() {
    saut = 1;
    let counterSaut:number = 0
    let sautInterval = setInterval(function(){
        let persoTop = parseInt(window.getComputedStyle(perso).getPropertyValue("top"));
        if((persoTop > 6) && (counterSaut < 20)){
            if(perso)
            perso.style.top = (persoTop-5)+"px";
        }     
        if(counterSaut > 20){
           clearInterval(sautInterval);
           saut = 0;
           counterSaut=0;
        }
        counterSaut++;
    },10);
}

// Cette fonction permet de faire sauter la balle en cliquant sur le body
document.body.addEventListener('click', (event) => {
    event.preventDefault()
    console.log('clic');
    Sauter();
    
})

